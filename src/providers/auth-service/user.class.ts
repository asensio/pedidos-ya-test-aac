export class User{
  public id: number;
  public name: string;
  public lastname:string;
  public country: Object;
  public email: string;
  public password: string;
  public token: string;
  
  constructor(email: string = '',  password: string= '', token: string = ''){
    this.email = email;
    this.password = password;
    this.token = token;
    this.country = { id : 1 };
  }

  /**
   * getId
   */
  public getId():number {
    return this.id;
  }
  /**
   * setId
   */
  public setId(id:number) {
    this.id = id;
    return this;
  }

  /**
   * getName
   */
  public getName():string {
    return this.name;
  }
  /**
   * setName
   */
  public setName(name:string) {
    this.name=name;
    return this;
  }

  /**
   * getLastname
   */
  public getLastname():string {
    return this.lastname;
  }
  /**
   * setLastname
   */
  public setLastname(lastname:string) {
    this.lastname = lastname;
  }

  /**
   * getCountry
   */
  public getCountry():Object {
    return this.country;
  }

  /**
   * setCountry
   */
  public setCountry(country:Object) {
    this.country = country;
    return this;
  }
  
  /*
   * getEmail
   */
  public getEmail():string {
    return this.email;
  }

  /**
   * setEmail
   */
  public setEmail(email:string) {
    this.email=email;
    return this;
  }

  /**
   * setPassword
   */
  public setPassword(password:string) {
    this.password = password;
    return this;
  }

  /**
   * getUserToken
   */
  public getToken() {
    return this.token;
  }
  
  /**
  * setToken
  */
  public setToken(token:string) {
    this.token = token;
    return this;
  }

}