import {User} from './user.class';

export class Api{
  _baseUrl: string;
  _clientId: string;
  _clientSecret: string;
  _userName: string;
  _password: string;
  _accessToken: string;
  _hasPermission: boolean;
  _user: User
  _country: string;
  _point: string;
  _search: string;

  constructor(){
    //
    /**
     *this._baseUrl = 'http://DIR_DEL_API.com/public/v1/';  
     */
    this._baseUrl = '';
    this._clientId= 'clientId=test';
    this._clientSecret = 'clientSecret=PeY@@Tr1v1@943';
    this._userName = 'userName=';
    this._password='password=';
    this._country='country=';
    this._point='point=';
    this._search='search/restaurants?';
    this._hasPermission = false;
  }

  /**
   * Get Api base url
   */
  public getBaseUrl(){
    return this._baseUrl;
  }

  /**
   * Return url to get token 
   */
  public requestAccessToken() {
    return this._baseUrl +'tokens?'+ this._clientId + '&' + this._clientSecret; 
  }

  /**
   * Return url to get user credentials 
   */
  public requestCredential(credentials) {
    return this._baseUrl + 'tokens?' + this._userName + credentials.email + 
    '&' + this._password + credentials.password; 
  }

  /**
   * Return url to get user credentials 
   */
  public requestMyAccount() {
    return this._baseUrl + 'myAccount'; 
  }

  

  /**
   * Return restaurant list 
   */
  public requestSearchRestaurants(id, latitude, longitude) {
    return this._baseUrl + this._search + this._country + id + 
    '&' + this._point + latitude+',' + longitude;
  }
  
  public setAccessToken(_accessToken:string){
      this._accessToken = _accessToken;
      this._hasPermission = true;
  }
  public getAccessToken(){
      return this._accessToken;
  }

  /**
   * If have an access token hasPermission will be true
   */
  public hasPermission() {
    return this._hasPermission;
  }
}