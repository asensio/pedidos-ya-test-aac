import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Api } from './api.class';
import { User } from './user.class';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthServiceProvider{

  _currentUser: User;
  _api:Api;

  constructor(public http: Http) {
    this._api = new Api();
    this._currentUser = new User('', '');
  }

  /**
   * Get access token
   */
  public __init(){
    this.http.get(this._api.requestAccessToken())
    .subscribe((res:Response)=>{
      let r = res.json();
      if(res.status == 200){
        if(r.access_token != undefined){
          window.localStorage.setItem('access_token', r.access_token);
          this._api.setAccessToken(r.access_token);
          console.log('App is authenticated');
        }
      }
    }, (error)=>{
      console.log(error);    
    })
    ;
  }

  public login(credentials){
    if(credentials.email == null && credentials.password == null && !this._api.hasPermission())
      return Observable.throw('Please insert credentials');
    else{
      var headers = new Headers();
      headers.append('Content-type','application/X-www-form=urlencoded');
      headers.append('Authorization','Bearer ' + this._api.getAccessToken());
      return Observable.create(observer => {
        let url = this._api.requestCredential(credentials);
        let requestOption = new RequestOptions({headers});
        let access = false;
        this.http.get(url, requestOption).subscribe((data)=>{
          let req = data.json();
          if(data.ok){
            access = true;
            this._currentUser = new User(credentials.email,credentials.password, req.access_token);
            window.localStorage.setItem('user', JSON.stringify(this._currentUser));
          }
          observer.next(access);
          observer.complete();
        }, error => {
            observer.next(access);
            observer.complete();
        });
      });
    }
  }

  public register(credentials){
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getCurrentUser():User {return this._currentUser;}
  
  public getUserLogged():User {
    this._currentUser = JSON.parse(window.localStorage.getItem('user'));
    return this._currentUser != undefined ? this._currentUser : null;;
  }

  public logout(){  
    return Observable.create(observer => {
      this._currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }






}
