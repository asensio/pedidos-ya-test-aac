/**
 * @author Ing. Abelardo Asensio Callol
 */
export class MapProvider{

    private map: any;
    private lat: number;
    private lng: number;
    private nativeElement: Element;
    private position: any;
    private bounds: any;
    private area: any;
    private markers;
    
    constructor(){
        this.getDefaultPosition();
        this.bounds = new google.maps.LatLngBounds();
        this.markers = [];
    }

    /**
     * This initialize a Map
     * @param lat Latitude value
     * @param lng Longitude value
     * @param mapElement Corresponding with the DOM element to render the Map
     */
    buildMap(lat:number|null, lng:number|null, mapElement){
        this.position= new google.maps.LatLng(
            (lat == null) ? this.lat : lat,
            (lng == null) ? this.lng : lng
        );
        this.bounds.extend(this.position);
        let mapOptions = {
            center: this.position,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            disableDefaultUI: true,
            scrollwheel: false
        };
        this.nativeElement = mapElement;
        this.map = new google.maps.Map(this.nativeElement, mapOptions);
        return this;
    }

    /**
     * Add new Marker to the Map
     * @param position Is a google.maps.LatLng objet
     * @param infoWindowContent The content to show when you click the pin
     */
    addMarker(position:any|null, infoWindowContent:string, title:string|''){
        let marker = new google.maps.Marker({
                position: (position == null) ? this.position : position, 
                map: this.map,
                title: title
            });
        let infoWindow = new google.maps.InfoWindow({
            content: infoWindowContent
        });
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.open(this.map, marker);
        });
        this.markers.push(marker); 
        return this;
    }

    /**
     * This will add multiples marker to the Map
     * @param locations An array of objects with {name: '', latitude: '', longitude: ''} values
     */
    addMultiplesMarker(locations: Array<{}>){
        let i, position;
        for (i = 0; i < locations.length; i++) {
            position = new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']);
            this.addMarker(position,locations[i]['name'], locations[i]['name']);
        }
        return this;
    }

    clearAll(){
        if(this.markers){
            for (var key in this.markers)
                this.markers[key].setMap(null);
        }
        if(this.area)
            this.area.setMap(null);
        return this;
    }

    getLat(){
        return this.lat;
    }

    getLng(){
        return this.lng;
    }

    getAll(){
        this.clearAll();
        let items = JSON.parse(window.localStorage.getItem('items'));
        let locations = new Array<{}>();
        items.forEach(element => {
            let points = element.coordinates.split(',');
            locations.push({
            name: element.name,
            latitude: parseFloat(points[0]),
            longitude: parseFloat(points[1])
            });
        });
        this.addMultiplesMarker(locations);
        return this;
    }

    getDefaultPosition(){
        this.lat = -34.874473;
        this.lng = -56.1961527;
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(position => {
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;         
            });
        }
        return new google.maps.LatLng(this.lat, this.lng);
    }
    showArea(){
        this.area = new google.maps.Circle({
            center: this.position,
            radius: 500,
            strokeColor: "#0000FF",
            strokeOpacity: 0.5,
            strokeWeight: 2,
            fillColor: "#0000FF",
            fillOpacity: 0.2
        });
        this.area.setMap(this.map);
        return this;
    }

    boundsChange(){
        //Center the Map
        this.map.fitBounds(this.bounds);
         // Override our map zoom level once our fitBounds function runs
        let boundsListener = google.maps.event.addListener((this.map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });
        return this;
    }

}