import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('dashboard') nav: Nav;
  rootPage:any = 'LoginPage';
  pages: Array<{}>;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.pages = [
      {icon: 'home', link: 'HomePage', title: 'Inicio' },
      {icon: 'navigate', link: 'MapPage', title: 'Ver en map' },
      {icon: 'contact',  link: 'AboutPage', title: 'Sobre mi' },
      {icon: 'log-out',  link: 'Logout', title: 'Salir' }
    ];
  }
 
  openPage(link){
    if(link == "Logout"){
      window.localStorage.removeItem('access_token');
      window.localStorage.removeItem('user');
      window.localStorage.removeItem('items');
      link = 'LoginPage';
    }
    this.nav.setRoot(link);
  }
}

