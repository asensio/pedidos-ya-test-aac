import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController,Platform, IonicPage, NavParams, ActionSheetController } from 'ionic-angular';
import { MapProvider } from '../../providers/map.class';
/**
 * Generated class for the MapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage implements OnInit{
  @ViewChild('map') mapElement;
  map: MapProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public actionSheetCtrl: ActionSheetController, public platform:Platform) { }

  ngOnInit(){ this.map = new MapProvider(); }

  ngAfterViewInit(){
    this.initMap();
  }

  /**
   * This is using Google Maps Web Api
   */
  initMap(){
    this.map.buildMap(null,null, this.mapElement.nativeElement)
    .addMarker(null,'Hola :) Te encuentras aquí!','')
    .boundsChange();
  }

  openMenu() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones de búsqueda',
      buttons: [
        {
          text: 'Ver todos',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'list' : null,
          handler: () => {
            this.map.getAll();
          }
        },{
          text: 'Cerca de mi',
          icon: !this.platform.is('ios') ? 'locate' : null,
          handler: () => {
            this.map.getAll()
            .addMarker(this.map.getDefaultPosition(),'','')
            .showArea();
          }
        },{
          text: 'Borrar todos',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            this.map.clearAll()
            .addMarker(this.map.getDefaultPosition(),'','');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
