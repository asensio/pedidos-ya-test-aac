import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, Loading, IonicPage, MenuController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { User } from '../../providers/auth-service/user.class';
import { Api } from '../../providers/auth-service/api.class';
import { MapProvider } from '../../providers/map.class';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  map: MapProvider;
  pages: Array<{}>;
  currentUser: User;
  _api: Api;
  list = [];
  loading: Loading;
  constructor(private nav: NavController,public menu: MenuController,
    private alertCtrl: AlertController,
    private auth: AuthServiceProvider, public http: Http) { }

  ngOnInit(){
    this.currentUser = new User();
    this._api = new Api();
    this.menu.enable(true);
    this.pages = [
      {icon: 'search', link: '', title: 'Search in map' },
      {icon: 'heart', link: '', title: 'Favorites' },
      {icon: 'contact', link: '', title: 'About me' }
    ];
    this.map = new MapProvider();
    this._myAccount();
    this._restaurantList('1', this.map.getLat(), this.map.getLng());
  }

  private _myAccount(){
    //Requesting for account values
    let userLogged = this.auth.getUserLogged();
    let url = this._api.requestMyAccount();
    let headers = new Headers();
    headers.append('Content-type','application/X-www-form=urlencoded');
    headers.set('Authorization','Bearer ' + userLogged['token']);
    let requestOption = new RequestOptions({headers});
    this.http.get(url, requestOption).subscribe((data)=>{
      let account = data.json();
      if(data.ok){
        this.currentUser.setId(account.id);
        this.currentUser.setName(account.name);
        this.currentUser.setLastname(account.lastName);
        this.currentUser.setCountry(account.country);
        this.currentUser.setEmail(userLogged['email']);
        this.currentUser.setPassword(userLogged['password']);
        this.currentUser.setToken(userLogged['token']);
        window.localStorage.setItem('user', JSON.stringify(this.currentUser));
      }
      else {
        this.showError("Error obteniendo datos");
      }
    },
    error => {
      this.showError(error);
    });
  }

  private _restaurantList(id, latitude, longitude, offset=0){
    //Requesting for restaurant list
    let userLogged = this.auth.getUserLogged();
    let url = this._api.requestSearchRestaurants(id, latitude, longitude);
    url += '&max=20'+'&offset='+ offset;
    let headers = new Headers();
    headers.append('Content-type','application/X-www-form=urlencoded');
    headers.set('Authorization','Bearer ' + userLogged['token']);
    let requestOption = new RequestOptions({headers});
    this.http.get(url, requestOption).subscribe((data)=>{
      if(data.ok){
        let values = data.json().data;
        if(offset == 0)
          this.list = values;
        else{
          values.forEach(element => {
            this.list.push(element);
          });
        }
        window.localStorage.setItem('items', JSON.stringify(this.list));
      }
      else {
        this.showError("Error obteniendo datos");
      }
    },
    error => {
      this.showError(error);
    });
  }

  cardDetail(id){
    let obj = this.list.find(item=>item.id == id);
    this.nav.push('DetailPage',{obj: obj});
  }

  public logout() {
    this.auth.logout().subscribe(succ => {
      this.nav.setRoot('LoginPage')
    });
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this._restaurantList('1', this.map.getLat(), this.map.getLng(), this.list.length);
      infiniteScroll.complete();
    }, 500);
  }

  showError(text) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

}