import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NavController, IonicPage, NavParams } from 'ionic-angular';
import { MapProvider } from '../../providers/map.class';


@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  @ViewChild('map') mapElement;
  logo:string;
  link: string;
  item: {};
  randomId: number;
  showToolbar:boolean;
  headerImgSize:string;
  headerImgUrl:string;
  transition:boolean;
  map: MapProvider;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public ref:ChangeDetectorRef) { }

  ngOnInit(){
    this.item = this.navParams.get('obj');
    this.showToolbar = false;
    this.headerImgSize = '100%';
    this.transition = false;
    this.map = new MapProvider();
  }

  ionViewDidLoad() {
     this.headerImgUrl = 'http://lorempixel.com/400/200/food/' + Math.floor((Math.random() * 10) + 1 );
     this.logo = 'https://d1v73nxuzaqxgd.cloudfront.net/restaurants/'+this.item['logo'];
     this.link = 'https://www.pedidosya.com.uy/restaurantes/montevideo/'+this.item['link']+'-menu';
     this.initMap();
  }
  onScroll($event: any){
    let scrollTop = $event.scrollTop;
    this.showToolbar = scrollTop >= 120;
    if(scrollTop < 0){
        this.transition = false;
        this.headerImgSize = `${ Math.abs(scrollTop)/2 + 100}%`;
    }else{
        this.transition = true;
        this.headerImgSize = '100%'
    }
    this.ref.detectChanges();
  }

  initMap(){
    this.map.buildMap(null,null, this.mapElement.nativeElement)
    .addMarker(null,'<div>Visitar</div><a href="'+this.link+'" target="blank">'+this.item['name']+'</a>','Pedidos Ya');
  }
}
