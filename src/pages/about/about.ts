import { Component } from '@angular/core';
import { NavController,IonicPage, NavParams } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  user: {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user = {};
  }

  ionViewDidLoad() {
    let wl = window.localStorage.getItem('user');
    if(wl)
      this.user = JSON.parse(wl);
  }

}
